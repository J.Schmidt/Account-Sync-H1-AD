### Das Account Programm dient der Synchronisation von HisInOne Datenbank und ActiveDirectory.

Hauptsächlich dient das Script der Übernahme der neu immatrikulierten Studenten in das ActiveDirectory. Dazu wird es in regelmäßigem Abstand (z.B. 15 min) ausgeführt.

Es wird die Programmiersprache Scala verwendet. Übersetzen mit

* **sbt assembly**

im Programmverzeichnis. Die alle erforderlichen Daten für einen Start des Programms mit Java (java -jar account_assembly-0.2.0.jar) beinhaltende jar-Datei entsteht im Verzeichnis target/scala-2.11 (sofern Scala Version 2.11 eingesetzt wird). Das ist der Weg, wenn nur die jar Datei weitergegeben werden soll. Über conf/setting.conf ist auch in diesem Fall eine Parametrisierung möglich. 

Auf dem Entwicklungsrechner wird der normale Weg des Programmstarts verwendet:

* **sbt compile**
* **sbt run**

Das Objekt LdapDB übernimmt die Kommunikation mit ActiveDirectory.\
Das Objekt WsClient stellt grundlegende Funktionen für die Kommunikation mit HisInOne per Webservice zur Verfügung.\
Das Objekt Logger realisiert das Logging in einen Logfile.\
Das Objekt config realisert die Konfiguration des Programms - Auswertung der Datei conf/setting.conf bzw. interne Daten.\
Das Objekt SoapReader ist das eigentliche Programm.\

Die Konfigurationsparameter
- AD_host, AD_admin, AD_password

sind für den Zugang zum Active Directory zuständig.

Die Konfigurationsparameter
- HisInOne_host, Soap_user, Soap_pw

sind für den Webservice Zugriff auf HisInOne zuständig.

Diese Parameter müssen in den Dateien config.scala bzw. setting.conf den Gegebenheiten angepasst werden. Beispiele sind in ~.example hinterlegt.

Für alle diese Parameter sind Standardwerte im Programm hinterlegt. Müssen diese geändert werden, so kann das in der Datei conf/setting.conf erfolgen, ohne das Programm ändern zu müssen. Dabei sind wie üblich mit # versehene Zeilen Kommentare. Die Parameter in der Datei überschreiben die im Programm hinterlegten Parameter.
Die weiteren Parameter dienen Testzwecken.

Die im Hauptobjekt main von SoapReader verwendeten Methoden sind:

* getStudentRoleId
* getStudentList
* isStudent
* getPersonInfo
* getAccountInfo

#### Funktionsprinzip:

Es werden alle Personen (Studenten und Bewerber) in HisInOne ermittelt, deren Daten in einem zurückliegenden Zeitraum Veränderungen unterlagen (getStudentList). Der Standard des Zeitraumes sind dabei 2 Tage. Durch den Kommandozeilenparameter -days 20 kann dieser Wert auf z.B. 20 Tage zurückliegend gesetzt werden.
Anschließend wird diese Liste auf die Liste der echten (d.h. immatrikulierten) Studenten reduziert unter Einsatz der Filterfunktion mit isStudent.
Danach erfolgt die Reduktion auf die Liste der Studenten mit korrektem Account (genau ein).

Ermittelte Unstimmigkeiten werden in den Log geschrieben. Ein zweiter Account wird akzeptiert, wenn er im AD vorhanden ist.

Für alle Studenten dieser nunmehr reduzierten Liste wird im AD abgefragt, ob bereits ein Account existiert. Diese Abfrage erfolgt zweistufig, da evtl. erneut immatrikulierte Studenten bereits aus der OU "studenten" entfernt wurden, aber noch im AD in einer anderen OU vorhanden sind. Daraus entstehen 2 Listen: listToCreate und listToMove.

Die Studenten der Liste listToCreate werden im AD angelegt. ldap.addUser trägt die Grunddaten im AD ein. shellCmd.addAccount erzeugt über Powershell das Exchange Konto - nur deshalb muss das Programm auf dem Exchange Server laufen.

Die Studenten der Liste listToMove werden zurück in die OU "studenten" verschoben und einschränkende Parameter werden zurück genommen. Außerdem werden sie in den Log geschrieben.
