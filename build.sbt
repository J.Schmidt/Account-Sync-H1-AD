organization := "Viadrina"

name := "Account"

version := "0.2.0"

scalaVersion := "2.13.6"

libraryDependencies += "org.scala-lang.modules" %% "scala-xml" % "2.0.0"

// scalacOptions ++= Seq("-unchecked", "-deprecation")
scalacOptions += "-deprecation"
