/* Minimalist SOAP client
 *
 * Authors:
 *   Bob Jamison
 *
 * Copyright (C) 2010 Bob Jamison
 * 
 *  This file is part of the Pedro library.
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 3 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
//package main.scala
import java.text.SimpleDateFormat
import java.util.Calendar
import jdbc._
import scala.xml.Node

object SoapReader {
//  val host = "http://h1-debian.europa-uni.de/qisserver/services2/"
//  val host = jdbc.config.getProperty("HisInOne_host")
//  val host = "http://hisinone-cust.europa-uni.de/qisserver/services2/"
//  val host = "https://viacampus.europa-uni.de/qisserver/services2/"

import java.nio.file.Paths
  val classPath = System.getProperty("java.class.path")
//  println("classPath: " + classPath)
//  val fileEncoding = System.getProperty("file.encoding")
//  println("fileEncoding: " + fileEncoding)
//  val consoleEncoding = System.getProperty("console.encoding")
//  println("consoleEncoding: " + consoleEncoding)
//  val tPath0 = Paths.get(classPath) //.getParent()
//  val tPath1 = if (!classPath.isDirectory) tPath0.getParent else tPath0
//  val tPath = tPath0.getParent
//  val fileName = tPath.getNameCount
//  println("getParent: " + tPath + " fileName: " + fileName)
//  val jksPath = tPath
//  val jksPath = Paths.get(Paths.get(classPath).getRoot.getParent.toString,"lib","viacampus.jks")
//  println("jkspath " + jksPath)
//  val input = ClassLoader.getSystemClassLoader.getResource("main/resources/viacampus.jks")
//  val input = Source.fromURL(getClass.getResource("main/resources/viacampus.jks"))
//  if (input != null) println("getResource: " + input)

//  System.setProperty("javax.net.ssl.trustStore", "conf/viacampus.jks");
//  System.setProperty("javax.net.ssl.trustStorePassword", "viacampus");
//  System.setProperty("javax.net.ssl.trustStoreType", "jks");

  //  System.setProperty("javax.net.ssl.keyStore", input.toString);
//  System.setProperty("file.encoding", "UTF-8")
//  System.setProperty("console.encoding", "UTF-8")
/*
  System.setProperty("javax.net.ssl.keyStore", "conf/viacampus.jks");
  System.setProperty("javax.net.ssl.keyStorePassword", "viacampus");
  System.setProperty("javax.net.ssl.keyStoreType", "jks");
*/
//  System.setProperty("javax.net.debug", "ssl");
//  sys.exit(0)
  
  val studentList, studentIdList = collection.mutable.Map[Int,Int]()
  val accountList = collection.mutable.Map[Int,Option[Node]]()
  
//  val cli = new WsClient
//  val roleOpt = getStudentRoleId
//  if (roleOpt.isEmpty) { println("init error"); exit(1) }
  
  val Seq(studentRole, staffRole) = Seq(Role.student, Role.mitarbeiter).map {
    getRoleId(_).getOrElse { println("init error"); sys.exit(1) }
  }
  
//  def getIdList(name: String): Option[Seq[Int]] =
//    getIdList(name, "")
/*
  def getIdList(name: String, date: String = ""): Option[Seq[Int]] = {
  val elem_1 = <per:surname>{name}</per:surname>
  val elem_2 = if (date != "") <per:updatedAt>{">=" + date}</per:updatedAt> else ""

  val req = <per:findPerson xmlns:per="http://www.his.de/ws/PersonService">
            {elem_1}
        {elem_2}
          </per:findPerson>
    val resp = WsClient.sendMessage(Service.person, req, Func.findPerson)
    resp.map(xml => (xml \\ "id").map(x => augmentString(x.text).toInt))
  }
*/
  def getRoleId(roleName: Role.Value): Option[String] = {
    val req = <per:findRoles60 xmlns:per="http://www.his.de/ws/RightsAndRolesService">
            <per:uniquename>={roleName}</per:uniquename>
          </per:findRoles60>
    val resp = WsClient.sendMessage(Service.rightsAndRoles, req, Func.findRoles60)
    resp.map(xml => (xml \\ "roleId").map(_.text))
      .filter(roleList => {
        val unique = (roleList.length == 1)
        if (!unique) println("roleId not unique for " + roleName)
        unique
      })
      .map(_.head)
  } 
  
  def getStudentList(name: String, date: String): Option[Seq[Int]] = {
//	val elem_1 = <per:person.surname>{name}</per:person.surname> // for findStudent
    val elem_1 = <per:surname>{name}</per:surname> // changed for findStudent61
    val elem_2 = if (date != "") <per:updatedAt>{">=" + date}</per:updatedAt> else ""

//	the order of Elements is relevant!
/*				{elem_1}
        <per:studystatus>1,2</per:studystatus>
        {elem_2}
*/
    val form = new SimpleDateFormat("dd.MM.yyyy")
    val today = form.format(Calendar.getInstance.getTime)
//	check now for "valid" students only
    val req = <per:findStudent61 xmlns:per="http://www.his.de/ws/StudentService">
        {elem_1}
        <!--per:studystatus>1,2,3</per:studystatus-->
        <per:disenrollmentDate>ISNULL</per:disenrollmentDate>
        <per:orgroleId>{studentRole}</per:orgroleId>
                <per:orgroleValidFrom>{"<=" + today}</per:orgroleValidFrom>
                <per:orgroleValidTo>{">" + today}</per:orgroleValidTo>
        {elem_2}
          </per:findStudent61>
    val resp = WsClient.sendMessage(Service.student, req, Func.findStudent61)
/*    if (resp.isDefined) {
//      val pp = new PrettyPrinter(80,4)
//      println("response:\n" + pp.format(resp.get))
      val studInfo = (resp.get \\ "studentInfo").filter(x => (x \\ "registrationnumber").text != "")
//      val idInt = studInfo.map(x => augmentString((x \\ "persionId").text).toInt)
      studInfo.foreach (x => {
        val id = augmentString((x \\ "persionId").text).toInt
        val mtknr = augmentString((x \\ "registrationnumber").text).toInt
        val studId = augmentString((x \\ "studentId").text).toInt
        studentList(id) = mtknr
        studentIdList(id) = studId
      })
      
//      studentList ++= idInt.zip(mtknrInt)
      Some (studentList.keys.toSeq)
    }
    else None
*/
    resp.map(xml => (xml \\ "studentInfo").filter(x => (x \\ "registrationnumber").text != "")
          .map (x => {
            val Seq(id, mtknr, studId) = Seq("persionId", "registrationnumber", "studentId")
              .map(y => (x \\ y).text)
              .map(augmentString(_).toInt)
            studentList(id) = mtknr
            studentIdList(id) = studId
            id
          })
    )
  }

  def getAltStudentList(date: String): Option[Seq[Int]] = {
//	val elem_1 = <per:person.surname>{name}</per:person.surname>
    val elem_1 = if (date != "") <per:updatedAt>{">=" + date}</per:updatedAt> else ""
//	val elem_1 = if (date != "") <per:orgroleValidFrom>{">" + date}</per:orgroleValidFrom> else ""

//	the order of Elements is relevant!
/*	order: updatedAt registrationStatus studystatusId roleId orgroleValidFrom			
 *    			{elem_1}
        <per:studystatus>1,2</per:studystatus>
        {elem_2}
*/
    val req = <per:findPerson60 xmlns:per="http://www.his.de/ws/PersonService">
        <!--per:registrationStatus>!=ISNULL</per:registrationStatus -->
        {elem_1}
        <per:studystatusId>1,2,3</per:studystatusId>
        <per:roleId>{studentRole}</per:roleId>
          </per:findPerson60>
    val resp = WsClient.sendMessage(Service.person, req, Func.findPerson60)
    resp.map(xml => (xml \\ "id").map(_.text).map(augmentString(_).toInt))
    
//      val pp = new   (80,4)
//      println("AltStudentList response:\n" + pp.format(resp.get))
//      val studentList = (resp.get \\ "id").map(_.text)
//      val idInt = studentList.map(augmentString(_).toInt)
//      println("altStudentList: " + studentList)
      
//    studentList ++= idInt.zip(mtknrInt)
  }

  def getStaffList(date: String): Option[Seq[Int]] = {
    val elem_1 = if (date != "") <per:updatedAt>{">=" + date}</per:updatedAt> else ""

//	the order of Elements is relevant!
//	order: updatedAt registrationStatus studystatusId roleId orgroleValidFrom			

    val req = <per:findPerson60 xmlns:per="http://www.his.de/ws/PersonService">
        {elem_1}
          <per:roleId>{staffRole}</per:roleId>
        </per:findPerson60>
    val resp = WsClient.sendMessage(Service.person, req, Func.findPerson60)
    resp.map(xml => (xml \\ "id").map(_.text).map(augmentString(_).toInt))
/*    if (resp.isDefined) {
//      val pp = new PrettyPrinter(80,4)
//      println("AltStudentList response:\n" + pp.format(resp.get))
      val staffList = (resp.get \\ "id").map(_.text)
//      println("staffList: " + staffList)
      val idInt = staffList.map(augmentString(_).toInt)

      Some (idInt)
    }
    else None
*/
  }

  def isValidStudent(id: Int): Boolean = {
    val form = new SimpleDateFormat("yyyy-MM-dd")
    val today = Calendar.getInstance.getTime
    val req = <per:readRolesForPerson xmlns:per="http://www.his.de/ws/RightsAndRolesService">
          <per:personId>{id}</per:personId>
        </per:readRolesForPerson>
    val resp = WsClient.sendMessage(Service.rightsAndRoles, req, Func.readRolesForPerson)
//    resp.map(xml => (xml \\ "roleId").filter(x => augmentString(x.text).toInt == studentRole))
    resp.map(xml => (xml \\ "orgRole").filter(x => {
      val Seq(validFrom, validTo) = Seq("validFrom","validTo").map(y => form.parse((x \\ y).text))
//      val pp = new scala.xml.PrettyPrinter(100,4)
//      println("isStudent response:\n" + pp.format(x))
      today.after(validFrom) && today.before(validTo) &&
        (x \\ "roleId").text == studentRole
    }))
    .filterNot(_.isEmpty)
    .isDefined
  }
  
//import scala.xml.PrettyPrinter
  private val roleResponseCache = collection.mutable.Map[Int,Option[xml.Elem]]()
  private def getRoleResponse(id: Int): Option[xml.Elem] = {
    val req = <per:readRolesForPerson xmlns:per="http://www.his.de/ws/RightsAndRolesService">
          <per:personId>{id}</per:personId>
         </per:readRolesForPerson>
    WsClient.sendMessage(Service.rightsAndRoles, req, Func.readRolesForPerson)
  }

  def getRoleList(id: Int): Seq[Int] = {
    val resp = roleResponseCache.getOrElseUpdate(id, getRoleResponse(id))
    resp.map(xml => (xml \\ "roleId").map(x => augmentString(x.text).toInt)).getOrElse(Seq())
  }
  
  def isValidStaff(id: Int): Boolean = {
    val form = new SimpleDateFormat("yyyy-MM-dd")
    val today = Calendar.getInstance.getTime
//    val resp = WsClient.sendMessage(Service.rightsAndRoles, req, Func.readRolesForPerson)
    val resp = roleResponseCache.getOrElseUpdate(id, getRoleResponse(id))
//    if (resp.isDefined) {
//      val pp = new scala.xml.PrettyPrinter(100,4)
//      println("isValidStaff response:\n" + pp.format(resp.get))
/*    val roleListOpt = resp.map(_ \\ "orgRole").filter(x => (x \\ "roleId").text == staffRole.toString)
//      println("isValidStaff response:\n" + pp.format(role.head))
    if (roleListOpt.isDefined) {
      val roleList = roleListOpt.get
      val Seq(validFrom, validTo) = Seq("validFrom","validTo").map(x => form.parse((roleList.head \\ x).text))
      today.after(validFrom) && today.before(validTo)
    } else false
*/
    resp.map(xml => (xml \\ "orgRole").filter(x => {
//      println("isValidStaff response:\n" + pp.format(role.head))
        val Seq(validFrom, validTo) = Seq("validFrom","validTo").map(y => form.parse((x \\ y).text))
        today.after(validFrom) && today.before(validTo) &&
          (x \\ "roleId").text == staffRole
    }))
    .filterNot(_.isEmpty)
    .isDefined
  }
  
//  cache for PersonInfos
  val personInfoList = collection.mutable.Map[Int,Option[Map[Person.Value,String]]]()

  def getPersonInfo(id: Int): Option[Map[Person.Value,String]] = {
    def getPersonInfo2(id: Int): Option[Map[Person.Value,String]] = {
       val req = <per:readPerson xmlns:per="http://www.his.de/ws/PersonService">
            <per:id>{id}</per:id>
          </per:readPerson>
      val resp = WsClient.sendMessage(Service.person, req, Func.readPerson)
      if (resp.isDefined) {
//      val attr = List("surname","firstname").foldLeft("")(extract)
//        Seq(Person.surname,Person.firstname,Person.dateofbirth).
//	    val personList = Map[Person.Value,String]()
//        Person.values.foreach(x => personList += (ArrowAssoc(x) -> (resp.get \\ x.toString).text))
        @annotation.tailrec
        def iter(valueList: Person.ValueSet, result: Map[Person.Value,String]): Map[Person.Value,String] = {
          if (valueList.isEmpty) result else {
            val x = valueList.head
           iter(valueList.tail, result + (ArrowAssoc(x) -> (resp.get \\ x.toString).text))
         }
        }
        Some (iter(Person.values, Map()))
      }
      else None
    } // end of getPersonInfo2
    def getPersonInfo3(id: Int): Option[Map[Person.Value,String]] = {
       val req = <per:readPerson xmlns:per="http://www.his.de/ws/PersonService">
              <per:id>{id}</per:id>
            </per:readPerson>
      val resp = WsClient.sendMessage(Service.person, req, Func.readPerson)
      resp.map(xml => Person.values.map(x => ArrowAssoc(x) -> (xml \\ x.toString).text).toMap)
    }
    personInfoList.getOrElseUpdate(id, getPersonInfo3(id))
  }
  
  def getAccountInfo(id: Int): Option[Node] = {
  def getAccountInfo2(id: Int): Option[Node] = {
    val req = <per:searchAccountForPerson xmlns:per="http://www.his.de/ws/AccountService">
            <per:personId>{id}</per:personId>
          </per:searchAccountForPerson>
      val resp = WsClient.sendMessage(Service.account, req, Func.searchAccountForPerson)
      resp.map(xml => // get all accounts of one person in one step
        (xml \\ "completeAccounts").head)
  }
  // caching of same id
  accountList.getOrElseUpdate(id, getAccountInfo2(id))
  }

  val form = new SimpleDateFormat("yyyy-MM-dd")
  val today = Calendar.getInstance.getTime
  
  def getStudentAccountInfo(id: Int): Option[Seq[String]] = {
// select students
    val accountLst = getAccountInfo(id).map(accounts => (accounts \\ "completeAccount")
        .filter(x => {
          val Seq(validFrom,validTo) = Seq("validFrom","validTo").map(y => form.parse((x \\ y).text))
          today.after(validFrom) && today.before(validTo)
        })
        .filter(x => {
          val uname = (x \\ "username").text
          uname.startsWith("euv") || !(LdapDB.userExistsInOU(uname,Seq(Ou.EUV_Mitarbeiter,Ou.Mitarbeiter))) 
        })
    ).getOrElse(Seq())
    val len = accountLst.length
    if (len == 1) {
      Some(Seq("username","isInitialpassword","passwordhash").map(x => (accountLst.head \\ x).text))
    }
    else { 
      println("mtknr: " + studentList(id) + " mit id: " + id + " hat unzulässige Anzahl von Accounts: " + len);
      Logger.put("<Fehler> mtknr: " + studentList(id) + " hat unzulässige Anzahl von Accounts: " + len)
      None
    }
  }
  
  def getStaffAccountInfo(id: Int): Option[Seq[String]] = {
// select staff
//      println("start getAccountInfo for: " + id)  
    val accountList = getAccountInfo(id).map(accounts => (accounts \\ "completeAccount").filter(x => {
      val blockedId = augmentString((x \\ "blockedId").text).toInt
      val uname = (x \\ "username").text
      val Seq(validFrom,validTo) = Seq("validFrom","validTo").map(y => form.parse((x \\ y).text))
      !uname.startsWith("euv") && (blockedId != 4) && // || !LdapDB.userExistsInOU(uname,"Studenten")
      today.after(validFrom) && today.before(validTo)
    })).getOrElse(Seq())
//    val uname = (getAccountInfo(id).get \\ "username").text
    val uname = getPersonInfo(id).get
//    println("getStaffAccountInfo: " + uname + " " + id)
    val len = accountList.length
    if (len == 1) Some(Seq("username","isInitialpassword","passwordhash").map(x => (accountList.head \\ x).text))
    else { 
//    println("mtknr: " + studentList(id) + " mit id: " + id + " hat unzulässige Anzahl von Accounts: " + len);
      val str = if (len == 0) " hat keinem Mitarbeiteraccount"
            else " hat unzulässige Anzahl von Accounts: " + len
      val str2 = "Mitarbeiter mit id: " + id + " name: " + (uname - Person.dateofbirth).values.mkString(", ") + str
      val roleList = getRoleList(id).filterNot(Seq(studentRole, staffRole).contains(_))
//      if (!roleList.isEmpty) println("roleList of id: " + id + " is: " + roleList)
      if (!roleList.isEmpty || len > 0) {
        println(str2);
        Logger.put("<Fehler> " + str2)
        val uidList = LdapDB.getUidFromName(uname(Person.firstname), uname(Person.surname))
        if (!uidList.isEmpty) {
          println("info from AD: " + uidList)
          Logger.put("Information vom AD: " + uidList)
        }
      }
      None
    }
  }
  
  def setAccount(id: Int, account: String): Option[String] = {
  val pwd = scala.util.Random.alphanumeric.take(6).mkString
  val req = <per:createNewAccountForPerson xmlns:per="http://www.his.de/ws/AccountService">
        <per:username>{account}</per:username>
        <per:password>{pwd}</per:password>
        <per:personId>{id}</per:personId>
          </per:createNewAccountForPerson>
//    val cli = new WsClient
//    println("Start getAccount")
    val resp = WsClient.sendMessage(Service.account, req, Func.createNewAccountForPerson)
/*    print("attr: ")
    List("personid","username","isLdapAccount").foreach { attr =>
      val atstr = (resp.get \\ attr).text
      print(atstr + ";")
    }
    println
*/
    if (resp.isDefined) {
//      def extract(result: String, attr: String) = {
//        (if (result.length > 0) result + ";" else result) + (resp.get \\ attr).text
//      }
//      val attr = List("username","personId").foldLeft("")(extract)
//      val attr = List("username","personId").map(x => (resp.get \\ x).text).mkString(";")
      val attr = (resp.get \\ "createNewAccountForPersonResponse").text
      Some(attr)
    }
    else None
  }

import java.util.{Date,Calendar}

  def main(args: Array[String]): Unit = {
    println("running on vm version: " + System.getProperty("java.runtime.version"));
//    val today = new Date //Calendar.getInstance().getTime()
    val days = if (args.length == 2) {
      if (args(0) == "-days") augmentString(args(1)).toInt else {
        println("Kommandozeilenargument für mehr (<> 2) Tage zurück: -days xx")
        2 
      }
    }
    else 2
    val cal = Calendar.getInstance()
//    cal.add(Calendar.DATE, 1 - cal.get(Calendar.DAY_OF_YEAR))
    cal.add(Calendar.DATE, -days)
    val form = new SimpleDateFormat("dd.MM.yyyy")
    val startDate = cal.getTime
    
//    println("studentRole: " + studentRole + " staffRole: " + staffRole)
    
    println ("starting date is: " + form.format(startDate))
//    println("year: " + cal.get(Calendar.YEAR))
//    sys.exit(0)
    
//    val option = getIdList("*",form.format(startDate))
    val option = getStudentList("*",form.format(startDate))
//    val option = getIdList("Sch*")
    val liste = option.getOrElse {println("keine sinnvolle Antwort"); Seq() } //.take(13)
    println("number: " + liste.length)
//    liste foreach (Console println "id: " + _)
    println()

/*	remove this because of giving incorrect data
    val altListOpt = getAltStudentList(form.format(startDate)) //.map(_.sorted)
    val altList = if (altListOpt.isDefined) {
      val al = altListOpt.get.sorted
      println("altList length: " + al.length)
      println("altList: " + al)
      al
    } else Seq()
*/
    
//        println("test: " + getPersonInfo(730))
    val liste0 = liste.filter(_>5)
//    val liste1 = liste0.filter(getPersonInfo(_).get.split(";").length > 1)
    val liste1 = liste0.filter(getStudentAccountInfo(_).isDefined).sorted
        //        val liste1 = liste0.filter(getAccountInfo(_).get.length > 0)
    println("number after remove (student role only): " + liste1.length)
//    println("studentList:\n" + liste1)

//  intermediate output for students -> remove later +++
//    liste1 foreach (x => Console println "attr: " + Seq(x,getPersonInfo(x).get,getStudentAccountInfo(x).get).mkString(";"))

//    form.format(startDate)
    val staffList0 = getStaffList(form.format(startDate)).getOrElse {
        println("error getting staffList")
        sys.exit(1)
    }.sorted
    
    println()
    println("length of staffList0: " + staffList0.length)
    
    val staffList1 = staffList0.filter(isValidStaff(_))
    println("length of staffList1: " + staffList1.length)
//    println
//    staffList foreach (x => Console println "attr: " + Seq(x,getPersonInfo(x).get,getStaffAccountInfo(x).get).mkString(";"))
    println()
    val staffList2 = staffList1.filter(x => {
      val username = getStaffAccountInfo(x).getOrElse(Seq(""))(0)
//      println("filter of: " + username + " " + LdapDB.userExistsInOU(username, Seq(Ou.EUV_Mitarbeiter,Ou.Mitarbeiter)))
//      if (username != "") !LdapDB.userExistsInOU(username, Seq(Ou.EUV_Mitarbeiter,Ou.Mitarbeiter,Ou.Tesa))
      if (username != "") !LdapDB.userExists(username)
      else false
    })
    println("staffList2 length: " + staffList2.length)
//    println("staffList1: " + staffList1)
//    staffList foreach (x => Console println "attr: " + Seq(x,getPersonInfo(x).get,getStaffAccountInfo(x).get).mkString(";"))

    println("Abgleich")
//  this is the List of Staff to load in AD
    val staffList = staffList2.filter(x => {
//    staffList foreach (x => {
      val personInfo = getPersonInfo(x).get
      val uidList = LdapDB.getUidFromName(personInfo(Person.firstname), personInfo(Person.surname))
      val notFound = (uidList.length == 0)
      if (notFound)
        Console println "personInfo: " + personInfo.values.mkString(", ")  + " " + getStaffAccountInfo(x).get + " " + uidList
      notFound
    })
    
    println()
    
    val listeAcc = liste1.filterNot(getStudentAccountInfo(_)	// use option
        .filter(x => LdapDB.userExistsInOU(x(0),Ou.Studenten)) // take first (0)
        .isDefined)
//    listeAcc foreach (x => Console println "username: " + getAccountInfo(x).get)
    listeAcc foreach (x => Console println "attr: " + studentList(x) + ";" + Seq(x,getPersonInfo(x).get.values.mkString(";"),getStudentAccountInfo(x).get).mkString(";"))
    println ("number after AD filter: " + listeAcc.length)
    val (listToMove,listToCreate) = listeAcc.partition(x => LdapDB.userExists(getStudentAccountInfo(x).get.head))
    println("to Create: " + listToCreate.length)
    println("to Move: " + listToMove.length)
//    ldap.addUser("euv-test3","Werner","Heisenberg","WH-123456")
//    sys.exit(0)

    val usernameCreateSet = collection.mutable.Set[String]()
    val noOutput = (jdbc.Config.getProperty("noADoutput") == "yes")
    listToCreate foreach { id => 
//    for (index <- 0 to math.min(1,listToCreate.length - 1)) {
//      val id = listToCreate(index)
      val personInfo = getPersonInfo(id).get
      val firstName = personInfo(Person.firstname )
      val lastName = personInfo(Person.surname )
      val accountInfo = getStudentAccountInfo(id).get
      val username = accountInfo(0)
//      val mtknr = studentList.get(id).get
      val mtknr = studentList(id)
/*      val password = (augmentString(firstName).take(1) + augmentString(lastName).take(1)).toUpperCase
      .replace('Ä','A')
      .replace('Ö','O')
      .replace('Ü','U') + "-" + any2stringfmt(mtknr).formatted("%05d")
*/
//    build password from known data
      val dayList = personInfo(Person.dateofbirth).split("-")
//      println("dayList: " + dayList(2))
      val day = dayList(2)
      val password = "S" + day + "-" + augmentString("%05d").format(mtknr)
//      println("Geburtstag: " + personInfo(Person.dateofbirth) + " select: " + day + " pw: " + password)
      if (username.startsWith("euv")) {
        val createstr = if (noOutput) "nicht erzeugt" else "erzeugt"
        if (!noOutput) {
          val success = LdapDB.addUser(username, firstName, lastName, password, Ou.Studenten)
          if (success) {
            ADAccess.addAccount(username)
            usernameCreateSet += username
          }
        }
        Logger.put(s"$username $mtknr $firstName $lastName $createstr")
      } 
      else { 
        val str = username + " nicht berücksichtigt!"
        println(str)
        Logger.put(str)
      }
    } // end of listToCreate loop

    staffList foreach {id =>  
      val personInfo = getPersonInfo(id).get
      val firstName = Utils.filter(personInfo(Person.firstname))
      val lastName = Utils.filter(personInfo(Person.surname))
      val accountInfo = getStaffAccountInfo(id).get
      val username = accountInfo(0)
      val password = accountInfo(2)
      if (!noOutput) {
        val success = LdapDB.addUser(username, firstName, lastName, password, Ou.EUV_Mitarbeiter)
        if (success) {
          ADAccess.addAccount(username)
          val str = "Mitarbeiter " + firstName + " " + lastName + " erzeugt"
          println(str)
          Logger.put("<Info> " + str)
        }
      }
    } // end of staffList foreach
    
    if (!noOutput) {
      ADAccess.close	// start PowerShell with collected data
//	  create Mail-Account if not already done
      val nomailStudentList = LdapDB.getNomailStudent(usernameCreateSet.toSet).filter(_.startsWith("euv"))
      if (nomailStudentList.length > 0) {
        if (ADAccess.addStudentAccountList(nomailStudentList.take(100))) {
          val extend = if (nomailStudentList.length > 10) " ..." else ""
          val str = "nachträglich mit Email versehen: " + nomailStudentList.take(10).mkString(",") + extend
          println(str)
          Logger.put("<INFO> " + str)
        }
      }
    }

    listToMove foreach { id =>
      val username = getStudentAccountInfo(id).get(0)
      if (username startsWith("euv")) { 
        println( "account to shift: " + username)
        Logger.put("Account zu verschieben: " + username)
        LdapDB.moveUser(username, Ou.Studenten)
      }
    } // end of listToMove
    Logger.close
  }
}