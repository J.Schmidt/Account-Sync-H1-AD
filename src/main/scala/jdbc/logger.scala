package jdbc

import java.util.Calendar
import java.text.SimpleDateFormat
//import java.io.{FileWriter, BufferedWriter}
import java.nio.file.{Paths,Files,StandardOpenOption}
import java.nio.charset.Charset

/*
 * collects all logstrings in logList and only makes output in "close" function
 * if there is something to output
 * log start and stop time are also logged only if there is output
 * if logfile and its directory doesn't exist they are created
 */

object Logger {	// scala object is a java static class
  
  val timeForm = new SimpleDateFormat("HH:mm:ss")
  val logList = collection.mutable.ListBuffer[String]()

  def put(str: String): Unit = {
    if (!oldContent.contains(str)) logList += str
  }

  def close: Unit = {
    if (!logList.isEmpty) {
//      val append = true
//      val writer = new BufferedWriter(new FileWriter(fileName.toString,append))
      val writer = Files.newBufferedWriter(fileName,Charset.defaultCharset,StandardOpenOption.APPEND)

      if (!oldContent.isEmpty) writer.newLine // start new block
      writer.write(logStartStr)
      writer.newLine
      for (str <- logList) {
        writer.write(str)
        writer.newLine
      }
      writer.write("log Ende: " + timeForm.format(Calendar.getInstance.getTime))
      writer.newLine
      
      writer.flush
      writer.close
    }
  }
  
  val today = Calendar.getInstance.getTime
  val dateForm = new SimpleDateFormat("yyyy_MM_dd")
  val dateStr = dateForm.format(today)
  val fileName0 = "log" + dateStr + ".log"
  val fileName = Paths.get(System.getProperty("user.home"),"accountlog",fileName0)
  val dirName = fileName.getParent()
  if (!Files.exists(dirName)) Files.createDirectory(dirName)
//	println("year: " + today.get(Calendar.YEAR))
  val logStartStr = "log Start: " + timeForm.format(today)
//	  val writer = new scala.io.File(Path("test.txt")).bufferedWriter
  if (!Files.exists(fileName)) Files.createFile(fileName)
  val oldContent = Files.readAllLines(fileName,Charset.defaultCharset())
}
