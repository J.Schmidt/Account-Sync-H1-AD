package jdbc

object Role extends Enumeration {
  val student, mitarbeiter = Value
}

object Ou extends Enumeration {
  val Studenten, Mitarbeiter, Tesa = Value
  val EUV_Mitarbeiter = Value("EUV Mitarbeiter")
  val EUV_Zwischen = Value("EUV Zwischen")
}

object Service extends Enumeration {
  val account = Value("AccountService")
  val person = Value("PersonService")
  val rightsAndRoles = Value("RightsAndRolesService")
  val student = Value("StudentService")
}

object Func extends Enumeration {
  val findStudent61, findRoles60, findPerson, findPerson60, readRolesForPerson = Value
  val readPerson, searchAccountForPerson, createNewAccountForPerson = Value
}

object Person extends Enumeration {
  val surname, firstname, dateofbirth = Value
}

object Utils {
  import java.text.Normalizer
  import util.matching.Regex
  def filter(str: String): String = {
    val sb = augmentString(str).map(ch => ch match {
      case 'Ä' => "Ae"
      case 'Ö' => "Oe"
      case 'Ü' => "Ue"
      case 'ä' => "ae"
      case 'ö' => "oe"
      case 'ü' => "ue"
      case 'ı' => "i"	// turkish
      case 'ł' => "l"	// polish
      case _ => ch
    })
    val str1 = Normalizer.normalize(sb.mkString,Normalizer.Form.NFD)
    new Regex("\\p{Mn}+").replaceAllIn(str1,"")	// Mn -> Non_Spacing_Mark remove
  }

  import scala.util.matching.Regex
  def pw_check(pw: String): Boolean = {
    val patternList = Seq(new Regex("\\d+"), new Regex("[A-Z]+"),
          new Regex("[a-z]+"), new Regex("[-+!@#$%&/=_.,:;]+"))
    @annotation.tailrec
    def check(regList: Seq[Regex], sum: Int): Boolean = {
      if (sum <= 0) true
      else {
        if (regList.isEmpty) false
        else {
          val sum1 = if (!regList.head.findFirstIn(pw).isEmpty) sum - 1 else sum
          check(regList.tail, sum1)
        }
      }
    }
    if (pw.length < 8) false else check(patternList, 3)
  }

}