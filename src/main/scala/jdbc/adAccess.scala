package jdbc
import sys.process._

/*
 * addAccount only fills two lists - one for students and one for staff
 * these lists are executed in function close:
 * private function closeFunc makes the common parts and
 * studentStr / staffStr are there for special settings
 * --> so there is only one PowerShell call per list, maximal 2 per program run
 */
object ADAccess {
  val upnStudentList, upnStaffList = collection.mutable.ListBuffer[String]()
  val studentStr = "Set-Mailbox -RecipientLimits 50 -ThrottlingPolicy LimitPolicy"
  val staffStr = "Set-Mailbox -Alias $Name -IssueWarningQuota 5120mb -ProhibitSendQuota 5222mb " +
    "-ProhibitSendReceiveQuota 5325mb -UseDatabaseQuotaDefaults $false -ThrottlingPolicy LimitMessagesSent"
  
  def addAccount(upn: String): Unit = {
    val tempList = if (upn.startsWith("euv")) upnStudentList else upnStaffList
    tempList += "'" + upn + "'"
  }
    
  def addStudentAccountList(upnList: Seq[String]): Boolean = {
    if (!upnList.isEmpty) closeFunc(upnList.map("'" + _ + "'"), studentStr)
    else true
  }

  def close: Unit = {
    if (!upnStudentList.isEmpty) {
      closeFunc(upnStudentList.toSeq, studentStr)
      upnStudentList.clear()
    }
    if (!upnStaffList.isEmpty) {
      closeFunc(upnStaffList.toSeq, staffStr)
      upnStaffList.clear()
    }
  }
  
  private def closeFunc(upnList: Seq[String], setupStr: String): Boolean = {
    val command = "PowerShell -command Get-PSSnapin -Registered -Name Microsoft.Exchange* | Add-PSSnapin; " +
        "ForEach ($Name in " + upnList.mkString(",") + ") {Get-User $Name | Enable-Mailbox " +
        "| " + setupStr + "}; exit"
//	    val command = "PowerShell -command Get-PSSnapin -Registered -Name Microsoft.Exchange* | Add-PSSnapin; " +
//	      "Get-User -RecipientType User -OrganizationalUnit Studenten " +
//	      "| Enable-Mailbox | Set-Mailbox -RecipientLimits 50 -ThrottlingPolicy LimitPolicy; exit"
    println("PowerShell command:\n" + command)
    try {
//	  val inputStream = new java.io.ByteArrayInputStream(commandStr.mkString("\n").getBytes("UTF-8"))
//	  val output = (stringToProcess(command) #< inputStream).lines_!
      val output = stringToProcess(command).!!
//  	  output.foreach(x => { println("ADAcess: " + x); if (x.length > 0) Logger.put(x) })
      if (output.length > 0) {
         println("ADAccess: " + output)
        Logger.put(output)
      }
      true
    }
    catch {
      case ex: Throwable => {
        println("Problem mit PowerShell Exchange, command:\n" + command + "\n" + ex)
        Logger.put("PS Problem: " + ex)
        false
      }
    }
  }
}