package jdbc

import java.util.Properties
//import java.io.IOException;
import javax.naming.directory.{SearchControls, DirContext}
import javax.naming.{Context, NamingException}
import javax.naming.ldap.{LdapContext, InitialLdapContext}
import java.util.{Calendar,GregorianCalendar}

object LdapDB {
//	val searchBase = "OU=Studenten"
  val Seq(adminName,adminPassword,ldapURL) = Seq("AD_admin","AD_password","AD_host").map(Config.getProperty(_))
  val searchCtls = new SearchControls;
  
  val actualYear = Calendar.getInstance.get(Calendar.YEAR)
  val ctx = init

//	some useful constants from lmaccess.h
  val UF_ACCOUNTDISABLE = 0x0002;
  val UF_PASSWD_NOTREQD = 0x0020;
  val UF_PASSWD_CANT_CHANGE = 0x0040;
  val UF_NORMAL_ACCOUNT = 0x0200;
  val UF_DONT_EXPIRE_PASSWD = 0x10000;
  val UF_PASSWORD_EXPIRED = 0x800000;
  
  def init: LdapContext = {
    val env = new Properties
    // 636 is ldaps
    //val ldapURL = "ldaps://10.173.74.128:636/DC=his-ad,DC=euv"
    //val ldapURL = "ldaps://viadc07.ad.uni-ffo.de:636/DC=ad,DC=uni-ffo,DC=de"
//		val ldapURL = "ldaps://ad.uni-ffo.de:636/DC=ad,DC=uni-ffo,DC=de"
//		val ldapURL = config.getProperty("AD_host")
    
    println("ldapURL: " + ldapURL + " admin: " + adminName)

    env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
//	    env.put(Context.SECURITY_PROTOCOL, "ssl");
    env.put(Context.SECURITY_AUTHENTICATION, "simple");
    env.put(Context.SECURITY_PRINCIPAL, adminName);
    env.put(Context.SECURITY_CREDENTIALS, adminPassword);

    // connect to my domain controller
    env.put(Context.PROVIDER_URL, ldapURL);

    // Create the search controls
    // Specify the search scope
    searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
    // Specify the Base for the search
    // searchBase = "DC=his-ad,DC=euv"; // now is in ldapURL
    // Specify the attributes to return
    //val returnedAtts =  Seq("mail","SN")
    //searchCtls.setReturningAttributes(returnedAtts.toArray);

//		Certificate is in viacampus.jks included
//		println("property: " + System.getProperty("javax.net.ssl.trustStore"))
//		System.setProperty("javax.net.ssl.trustStore", "conf/viacampus.jks" );
//		System.setProperty("javax.net.ssl.trustStorePassword", "viacampus" );
//		System.setProperty("javax.net.debug", "handshake")
    
    new InitialLdapContext(env, null)
  }

  def userExists(uid: String): Boolean = {
    ctx.search("", "sAMAccountName=" + uid, searchCtls).hasMoreElements
  }
 
  def userExistsInOU(uid: String, ou: Ou.Value): Boolean = {
    userExistsInOU(uid,Seq(ou))
  }
    
  def userExistsInOU(uid: String, ouList: Seq[Ou.Value]): Boolean = {
    @annotation.tailrec
    def iter(uid: String, ouL1: Seq[Ou.Value], res: Boolean): Boolean = {
      if (res || ouL1.isEmpty) res
      else {
        val result = ctx.search("OU=" + ouL1.head, "sAMAccountName=" + uid, searchCtls)
        iter(uid, ouL1.tail, result.hasMoreElements)
      }
    }
    iter(uid, ouList, false)
  }
/*
    def close {
    ctx.close
  }
*/
  import javax.naming.directory.{BasicAttribute, BasicAttributes}

  def addUser(username: String, firstName: String, lastName: String, password: String, targetOu: Ou.Value): Boolean = {
  /* throws NamingException */

    println("addUser Parameter: " + username + " " + firstName + " "
        + lastName + " " + password);
    // Create a container set of attributes
    val container = new BasicAttributes(true);

    // Create the objectclass to add
    val objClasses = new BasicAttribute("objectClass");
    objClasses.add("top");
    objClasses.add("person");
    objClasses.add("organizationalPerson");
    // objClasses.add("inetOrgPerson");
    objClasses.add("user");

    val email_ext = Config.getProperty("Mail_ext")
//	Assign the username, first name, and last name
//		val cnValue = new StringBuffer(lastName).append(", ").append(firstName)
//				.append("(" + username + ")").toString();
//		muss mit cnValue2 korrespondieren (zusätzliches Leerzeichen)
    val cnValue = lastName + ", " + firstName + cnValueExt(username,targetOu) //" (" + username + ")"
    val cn = new BasicAttribute("cn", cnValue);
    val givenName = new BasicAttribute("givenName", firstName);
    val sn = new BasicAttribute("sn", lastName);
    val uid = new BasicAttribute("sAMAccountName", username);

    val mail = new BasicAttribute("mail", username + email_ext)			// +++
    // "@his-ad.euv"
//		val upnValue = (if (targetOu == Ou.Studenten) username else lastName.toLowerCase) + email_ext 
    val upnValue = username + email_ext
    val userPrincipalName = new BasicAttribute("userPrincipalName", upnValue)
    val displayName = new BasicAttribute("displayName", lastName + ", " + firstName)
    val mailNickname = new BasicAttribute("mailNickname", username)

    val mDBUseDefaults = new BasicAttribute("mDBUseDefaults","TRUE")	// +++
    val homeMDB = new BasicAttribute("homeMDB","TRUE");					// +++

    // Add password
    //Attribute userPassword = new BasicAttribute("userpassword", password);
/*		Attribute userPassword = null;
    try {
      String newQuotedPassword = "\"" + password + "\"";
      userPassword = new BasicAttribute("unicodePwd", newQuotedPassword.getBytes("UTF-16LE"));
    }
    catch (IOException e) {
      System.err.println("Problem beim Passwortwandeln: " + e);
    }
*/
    val newQuotedPassword = "\"" + password + "\""
//		val newQuotedPassword = "\"" + "Sommer2018" + "\""
    val	userPassword = new BasicAttribute("unicodePwd", newQuotedPassword.getBytes("UTF-16LE"))

    // Add these to the container
    container.put(objClasses)
//		container.put("objectClass","user") // +++
    container.put(cn)
    container.put(sn)
    container.put(givenName)
    container.put(uid)
//		container.put(mail)				// +++
    container.put(userPrincipalName)
    container.put(displayName)
//		container.put(mailNickname)		// +++
//		container.put(mDBUseDefaults)	// +++
//		container.put(homeMDB);			// +++

//		container.put(
//				"userAccountControl",
//				Integer.toString(UF_NORMAL_ACCOUNT + UF_PASSWD_NOTREQD
//						+ UF_PASSWORD_EXPIRED + UF_ACCOUNTDISABLE));

    container.put("userAccountControl",UF_NORMAL_ACCOUNT.toString);
    val pw_valid = Utils.pw_check(password)
    if (pw_valid)
      container.put(userPassword)
    else {
      val str = "Passwort ungültig, nicht gesetzt!"
      println(str)
      Logger.put(str)
    }

    // Create the entry
/*		Context result = null;
    try {
      // baut den String so auf, wie er im AD sichtbar wird
      String cnValue2 = new StringBuffer(lastName).append("\\, ").append(firstName)
          .append("(" + username + ")").toString();
      //String cnValue2 = new StringBuffer("sAMAccountName=" + username + ",")
      //		.append("OU=Studenten").append(",")
      //		.append(searchBase).toString();
      //String cnValue2 = new StringBuffer(username + "@")
      //	.append("his-ad.euv").toString();

      result = ctx.createSubcontext(getUserDN(cnValue2), container);
      //result = ctx.createSubcontext("CN=" + cnValue2, container);
      //result = ctx.createSubcontext(cnValue2, container);
    } catch (NamingException e) {
      System.err.println("Problem bei der Object Erstellung: " + e);
    }
*/
    ouCreate(getUserOU(targetOu))
    
    val cnValue2 = lastName + "\\, " + firstName + cnValueExt(username, targetOu) // + " (" + username + ")"
    if (!pw_valid) false
    else
      try {
        val result = ctx.createSubcontext(getUserDN(cnValue2,targetOu), container);
        val retval = result != null 
        if (retval) println("Account erstellt: " + getUserDN(cnValue,targetOu));
        retval
      } catch {
        case e: NamingException => {
          val tempStr = "Problem bei der Objekt Erstellung für: " + cnValue + " " + e
          println(tempStr);
          Logger.put("<Fehler> " + tempStr)
          false
        }
      }
/*
      try {
        // Check that it was created by listing its parent
        NamingEnumeration<NameClassPair> list = ctx.list("OU=Studenten");

        // Go through each item in list
        int count = 0;
        while (list.hasMore() & count < 10) {
          NameClassPair nc = list.next(); count++;
          System.out.println(nc);
        } 
      } catch (NamingException e) {
      System.out.println("Problem bei Listenausgabe: " + e);
    }
*/		
  }

  val ouList = collection.mutable.ListBuffer[String]()	// OU cache
//	  only create/search targetOU once per run
  def ouCreate(userOU: String): Unit = {
    val userOUList = userOU.split(",", 2)
    val targetOU = userOUList(0)	// .head
    if(!ouList.contains(targetOU)) {
      val ouExist = ctx.search(userOUList(1), targetOU, searchCtls).hasMoreElements
      if (!ouExist) {
        val attrs = new BasicAttributes(true)
        val objclass = new BasicAttribute("objectClass")
        objclass.add("top"); objclass.add("organizationalUnit")
        attrs.put(objclass)
        val result = ctx.createSubcontext(userOU, attrs)
        println("OU: " + userOU + " created, result: " + result)
        println("name: " + result.getNameInNamespace)
      }
      ouList += targetOU
    }
  }
  
  def getUidFromName(firstName: String, lastName: String): Seq[String] = {
    @annotation.tailrec
    def getUidFromName(firstName: String, lastName: String, ouStr: Seq[Ou.Value], result: Seq[String]): Seq[String] = {
      if (ouStr.isEmpty) result
      else {
//	      val searchFilter = "(&(sn=" + lastName + ")(givenName=" + firstName + "))"
        val searchFilter = s"(&(sn=$lastName)(givenName=$firstName))"
        val altSearchCtls = searchCtls
        altSearchCtls.setReturningAttributes(Array("sAMAccountName"))
        val searchBaseStr = "OU=" + ouStr.head
    
        val answer = ctx.search(searchBaseStr, searchFilter, altSearchCtls)

        val uidList = new collection.mutable.ListBuffer[String]
        while (answer.hasMore) {
          uidList += answer.next.getAttributes.get("sAMAccountName").get(0).toString
        }
        getUidFromName(firstName,lastName,ouStr.tail,Seq.concat(result,uidList.toSeq))
      }
    } // end of iterating function getUidFromName(_,_,_,_)
    getUidFromName(firstName,lastName,Seq(Ou.EUV_Mitarbeiter,Ou.Mitarbeiter),Seq())
  }

  def getNomailStudent(justCreatedSet: Set[String]): Seq[String] = {
//    without objectClass=user there is a null pointer exception from java
    val searchFilter = "(&(objectClass=user)(!(mail=*)))"
    val altSearchCtls = searchCtls
    altSearchCtls.setReturningAttributes(Array("sAMAccountName"))
    val searchBaseStr = "OU=" + Ou.Studenten
    
    val answer = ctx.search(searchBaseStr, searchFilter, altSearchCtls)
    val uidList = new collection.mutable.ListBuffer[String]
      
    while (answer.hasMoreElements) {
      val username = answer.next.getAttributes.get("sAMAccountName").get(0).toString
        // maybe mail creation isn't ready yet
      if (!justCreatedSet.contains(username)) uidList += username 
    }
//      println("getNomailStudents number of Elements: " + uidList.length)
    uidList.toSeq
  }
  
  def moveUser(username: String, targetOU: Ou.Value): Unit = {
    val altSearchCtls = searchCtls
    altSearchCtls.setReturningAttributes(Array("distinguishedName"))
//	  val cn = ctx.getAttributes(username, Array("distinguishedName"))
    val answer = ctx.search("", "sAMAccountName=" + username, altSearchCtls)
    val dn0 = answer.next.getAttributes.get("distinguishedName").get.toString
    val dn = dn0.split(",DC=", 2)(0)	// second part not used!
    val newdn0 = dn.split(",OU=", 2)
    val (newdn, newdn1) = (newdn0(0), "OU=" + newdn0(1))

    val expDate = new GregorianCalendar(actualYear + 50, Calendar.JANUARY, 1)
//	  val expDate = Calendar.getInstance
//	  expDate.set(actualYear + 50, Calendar.JANUARY, 1)
    val w32Start = new GregorianCalendar(1601, Calendar.JANUARY, 1)
//	  val w32Start = Calendar.getInstance
//	  w32Start.set(1601, Calendar.JANUARY, 1)
    val ticks100ns = (expDate.getTimeInMillis - w32Start.getTimeInMillis) * 10000

    val attList = new BasicAttributes(true)
    attList.put(new BasicAttribute("accountExpires",ticks100ns.toString))
    
//	  reset Exchange Parameters AcceptMessagesOnlyFrom and HiddenFromAdressLists
    attList.put(new BasicAttribute("authOrig",null))
    attList.put(new BasicAttribute("msExchHideFromAddressLists","FALSE"))
//	  val UF_ACCOUNTDISABLE = 0x2  // remove AccountDisable attribute
    val userAccountControlOrig = augmentString(ctx.getAttributes(dn).get("userAccountControl").get().toString).toInt
//	  println("userAccountControlOrig: " + userAccountControlOrig)
    attList.put(new BasicAttribute("userAccountControl",(userAccountControlOrig & ~UF_ACCOUNTDISABLE).toString))
    
    try {
      ctx.modifyAttributes(dn, DirContext.REPLACE_ATTRIBUTE, attList)
      val newOuString = getUserOUMove(targetOU)
      ouCreate(newOuString)
      ctx.rename(dn, newdn + "," + newOuString)
      val str = "Account: " + username + " verschoben von: " + newdn1 + " nach: " + newOuString
      println(str)
      Logger.put("<Info> " + str)
    }
    catch {
      case e: NamingException => {
        val tempStr = "Problem bei der Objekt Verschiebung für: " + dn + " " + e
        println(tempStr);
        Logger.put("<Fehler> " + tempStr)
      }
    }
  }
//	use other OU on move
  def getUserOUMove(targetOu: Ou.Value): String = {
    getUserOU(targetOu, s"$actualYear-verschoben")
  }
  
  def getUserOU(targetOu: Ou.Value): String = {
    getUserOU(targetOu, actualYear.toString)
  }
  
  def getUserOU(targetOu: Ou.Value, testOU: String): String = {
    targetOu match {
      case Ou.Studenten => "OU=" + testOU + ",OU=" + targetOu
      case Ou.EUV_Mitarbeiter => "OU=" + Ou.EUV_Zwischen + ",OU=" + targetOu
    }
  }
  
  def getUserDN(cnValue: String, targetOu: Ou.Value) = "CN=" + cnValue + "," + getUserOU(targetOu)
  
  def cnValueExt(userName: String, targetOu: Ou.Value): String = {
    targetOu match {
      case Ou.Studenten => " (" + userName + ")"
      case Ou.EUV_Mitarbeiter  => ""
    }
  }

}